package com.example.quizapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HighScoreActivity extends AppCompatActivity {
    private TextView playerView,scoreView;
    private Button restartButton;
    private ListView highScores;
    private String name;
    private int score;
    ArrayList<Score> scoreList;
    private CustomAdapter customAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.highscore_layout);

        Bundle intent = getIntent().getExtras();
        name = intent.getString("PLAYER_NAME");
        score = intent.getInt("SCORE");

        //playerView = findViewById(R.id.textViewName);
        //playerView.setText(name);

        scoreView = findViewById(R.id.textViewScorePoints);
        scoreView.setText("Végső pontszám: " + String.valueOf(score));

        restartButton = findViewById(R.id.buttonRestart);
        restartButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                restartQuiz();
            }
        });

        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        Date date = new Date();
        String datum = dateFormat.format(date);

        ScoreDbHelper dbHelper = new ScoreDbHelper(this);
        dbHelper.addScore(new Score(name,score, datum));
        scoreList = dbHelper.getScores();

        highScores = findViewById(R.id.listViewScores);

        customAdapter = new CustomAdapter(this,scoreList);
        highScores.setAdapter(customAdapter);



    }

    private void restartQuiz(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}