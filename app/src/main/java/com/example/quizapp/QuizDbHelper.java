package com.example.quizapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.quizapp.QuizContract.*;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class QuizDbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "QuizApp.db";
    private static final int DATABASE_VERSION = 1;

    private SQLiteDatabase db;

    public QuizDbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        this.db = sqLiteDatabase;

        final String SQL_CREATE_QUESTIONS_TABLE = "CREATE TABLE " +
                QuestionsTable.TABLE_NAME + " ( " +
                QuestionsTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                QuestionsTable.COLUMN_QUESTION + " TEXT, " +
                QuestionsTable.COLUMN_OPTION1 + " TEXT, " +
                QuestionsTable.COLUMN_OPTION2 + " TEXT, " +
                QuestionsTable.COLUMN_OPTION3 + " TEXT, " +
                QuestionsTable.COLUMN_OPTION4 + " TEXT, " +
                QuestionsTable.COLUMN_ANSWER_NR + " INTEGER" +
                ")";
        db.execSQL(SQL_CREATE_QUESTIONS_TABLE);
        fillQuestionsTable();
    }

    private void fillQuestionsTable() {
        addQuestion(new Question("@drawable/question1", "5", "3","9","11",1));
        addQuestion(new Question("@drawable/question2", "3", "7","2","8",2));
        addQuestion(new Question("@drawable/question3", "23", "56","33","11",3));
        addQuestion(new Question("@drawable/question4", "12", "31","20","16",4));
        addQuestion(new Question("@drawable/question5", "25", "22","43","10",1));
        addQuestion(new Question("@drawable/question6", "1", "3","6","4",2));
        addQuestion(new Question("@drawable/question7", "2", "5","6","9",3));
        addQuestion(new Question("@drawable/question8", "2", "9","7","4",4));
        addQuestion(new Question("@drawable/question9", "19", "15","21","26",1));
        addQuestion(new Question("@drawable/question10", "10", "31","29","41",2));
        addQuestion(new Question("@drawable/question11", "12", "6","3","1",3));
        addQuestion(new Question("@drawable/question12", "10", "9","3","6",4));
        addQuestion(new Question("@drawable/question13", "15", "13","18","19",1));
        addQuestion(new Question("@drawable/question14", "2", "4","8","6",2));
        addQuestion(new Question("@drawable/question15", "8", "5","2","11",3));
        addQuestion(new Question("@drawable/question16", "3", "5","13","9",4));
        addQuestion(new Question("@drawable/question17", "225", "246","212","185",1));
        addQuestion(new Question("@drawable/question18", "15", "10","8","6",2));
        addQuestion(new Question("@drawable/question19", "13", "6","1","3",3));
        addQuestion(new Question("@drawable/question20", "113179825", "1131797125","1131696","113179711",4));
        addQuestion(new Question("@drawable/question21", "16", "20","10","6",1));
        addQuestion(new Question("@drawable/question22", "27", "32","43","14",2));
        addQuestion(new Question("@drawable/question23", "5", "2","-","1",3));
        addQuestion(new Question("@drawable/question24", "3", "1","2","-",4));
        addQuestion(new Question("@drawable/question25", "*", "**","***","-",1));
        addQuestion(new Question("@drawable/question26", "6", "1","3","9",2));
        addQuestion(new Question("@drawable/question27", "45679802", "456789140","45678910","4567892",3));
        addQuestion(new Question("@drawable/question28", "2", "7","3","1",4));
        addQuestion(new Question("@drawable/question29", "3", "6","8","1",1));
        addQuestion(new Question("@drawable/question30", "5+4+1+0", "5 + 4 + 1 + 0 +","5+4+1+0+","5 + 4 + 1 + 0",2));


    }


    private void addQuestion(Question question){
        ContentValues cv = new ContentValues();
        cv.put(QuestionsTable.COLUMN_QUESTION, question.getQuestion());
        cv.put(QuestionsTable.COLUMN_OPTION1, question.getAnswer1());
        cv.put(QuestionsTable.COLUMN_OPTION2, question.getAnswer2());
        cv.put(QuestionsTable.COLUMN_OPTION3, question.getAnswer3());
        cv.put(QuestionsTable.COLUMN_OPTION4, question.getAnswer4());
        cv.put(QuestionsTable.COLUMN_ANSWER_NR, question.getAnswerCorrect());
        db.insert(QuestionsTable.TABLE_NAME, null, cv);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ QuestionsTable.TABLE_NAME);
        onCreate(db);
    }

    public List<Question> getAllQuestions() {
        List<Question> questionList = new ArrayList<>();
        db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + QuestionsTable.TABLE_NAME, null);
        if (c.moveToFirst()) {
            do {
                Question question = new Question();
                question.setQuestion(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_QUESTION)));
                question.setAnswer1(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_OPTION1)));
                question.setAnswer2(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_OPTION2)));
                question.setAnswer3(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_OPTION3)));
                question.setAnswer4(c.getString(c.getColumnIndex(QuestionsTable.COLUMN_OPTION4)));
                question.setAnswerCorrect(c.getInt(c.getColumnIndex(QuestionsTable.COLUMN_ANSWER_NR)));
                questionList.add(question);
            } while (c.moveToNext());
        }
        c.close();
        return questionList;
    }
}
