package com.example.quizapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ComponentActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QuizActivity extends AppCompatActivity {
    private TextView playerNameView,scoreView;
    private ImageView questionView;
    private Button answerButton1,answerButton2,answerButton3,answerButton4;
    private List<Question> questionList;
    private int questionNumber = 1;
    private int score = 0;
    private int questionCounter = 0;

    private String playerName;

    private Question currentQuestion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quiz_layout);

        Bundle intent = getIntent().getExtras();
        playerName = intent.getString("PLAYER_NAME");

        playerNameView = findViewById(R.id.textViewPlayerName);
        playerNameView.setText("Játékos: " + playerName);

        scoreView = findViewById(R.id.textViewScore);
        scoreView.setText("Pontszám: " + score);

        questionView = findViewById(R.id.imageViewQuestion);

        answerButton1= findViewById(R.id.answerButton1);
        answerButton2= findViewById(R.id.answerButton2);
        answerButton3= findViewById(R.id.answerButton3);
        answerButton4= findViewById(R.id.answerButton4);

        QuizDbHelper dbHelper = new QuizDbHelper(this);
        questionList = dbHelper.getAllQuestions();

        Collections.shuffle(questionList);

        showNextQuestion();

        answerButton1.setOnClickListener(new View.OnClickListener(){
                                             @Override
                                             public void onClick(View view) {
                                                checkAnswer(1);
                                             }
                                         }

        );
        answerButton2.setOnClickListener(new View.OnClickListener(){
                                             @Override
                                             public void onClick(View view) {
                                                 checkAnswer(2);
                                             }
                                         }

        );
        answerButton3.setOnClickListener(new View.OnClickListener(){
                                             @Override
                                             public void onClick(View view) {
                                                 checkAnswer(3);
                                             }
                                         }

        );
        answerButton4.setOnClickListener(new View.OnClickListener(){
                                             @Override
                                             public void onClick(View view) {
                                                 checkAnswer(4);
                                             }
                                         }

        );
    }

    private void checkAnswer(int selectedAnswer){
        if(currentQuestion.getAnswerCorrect() == selectedAnswer){
            score++;
            scoreView.setText("Pontszám: " + score);
        }
        showNextQuestion();
    }


    private void showNextQuestion(){
        if (questionCounter < 10){
            currentQuestion = questionList.get(questionCounter);

            int imageResource = getResources().getIdentifier(currentQuestion.getQuestion(),null,this.getPackageName());
            questionView.setImageResource(imageResource);

            answerButton1.setText(currentQuestion.getAnswer1());
            answerButton2.setText(currentQuestion.getAnswer2());
            answerButton3.setText(currentQuestion.getAnswer3());
            answerButton4.setText(currentQuestion.getAnswer4());

            getSupportActionBar().setTitle("Kérdés: " + questionNumber +"/10");
            questionNumber++;
            questionCounter++;
        } else{
            finishQuiz();
        }
    }
    private void finishQuiz(){
        Intent intent = new Intent(this, HighScoreActivity.class);
        intent.putExtra("PLAYER_NAME", playerName);
        intent.putExtra("SCORE",score);
        startActivity(intent);
        finish();

    }
}