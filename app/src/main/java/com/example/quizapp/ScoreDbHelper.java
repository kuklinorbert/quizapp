package com.example.quizapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.quizapp.ScoreContract.*;

import androidx.annotation.Nullable;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class ScoreDbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "Scores.db";
    private static final int DATABASE_VERSION = 1;

    private SQLiteDatabase db;

    public ScoreDbHelper(@Nullable Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        this.db = sqLiteDatabase;

        final String SQL_CREATE_SCORES_TABLE = "CREATE TABLE " +
                ScoresTable.TABLE_NAME + " ( " +
                ScoresTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                ScoresTable.COLUMN_PLAYER + " TEXT, " +
                ScoresTable.COLUMN_SCORE + " INTEGER, " +
                ScoresTable.COLUMN_DATE + " TEXT" +
                ")";
        db.execSQL(SQL_CREATE_SCORES_TABLE);



    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + ScoresTable.TABLE_NAME);
        onCreate(db);
    }

    public void addScore(Score score){
        db = getWritableDatabase();
        insertScore(score);
    }

    private void insertScore(Score score){
        ContentValues cv = new ContentValues();
        cv.put(ScoresTable.COLUMN_PLAYER,score.getPlayerName());
        cv.put(ScoresTable.COLUMN_SCORE,score.getScore());
        cv.put(ScoresTable.COLUMN_DATE,score.getCompleted());
        db.insert(ScoresTable.TABLE_NAME, null, cv);
    }

    public ArrayList<Score> getScores(){
        ArrayList<Score> scoreList = new ArrayList<>();
        db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + ScoresTable.TABLE_NAME + " ORDER BY " + ScoresTable.COLUMN_SCORE + " DESC", null);
        if(c.moveToFirst()){
            do{
                Score score = new Score();
                score.setPlayerName(c.getString(c.getColumnIndex(ScoresTable.COLUMN_PLAYER)));
                score.setScore(c.getInt(c.getColumnIndex(ScoresTable.COLUMN_SCORE)));
                score.setCompleted(c.getString(c.getColumnIndex(ScoresTable.COLUMN_DATE)));
                scoreList.add(score);
            }while(c.moveToNext());
        }
        c.close();
        return scoreList;
    }
}

