package com.example.quizapp;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);
    }

    public void Login(View view){
        Intent intent = new Intent(this, QuizActivity.class);
        EditText editText = findViewById(R.id.editTextName);
        if(editText.getText().toString().isEmpty()){
            editText.setError(getString(R.string.input_empty));
            editText.requestFocus();
        }else {
            String name = editText.getText().toString();
            intent.putExtra("PLAYER_NAME", name);
            startActivity(intent);
            finish();
        }
    }

}