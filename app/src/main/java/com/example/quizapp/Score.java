package com.example.quizapp;

import java.time.LocalDateTime;

public class Score {
    private String playerName;
    private int score;
    private String completed;

    public Score(){}

    public Score(String playerName, int score, String completed) {
        this.playerName = playerName;
        this.score = score;
        this.completed = completed;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }
}
