package com.example.quizapp;

import android.provider.BaseColumns;

public class ScoreContract {
    private ScoreContract() {
    }
    public static class ScoresTable implements BaseColumns {
        public static final String TABLE_NAME = "scores";
        public static final String COLUMN_PLAYER = "playerName";
        public static final String COLUMN_SCORE = "score";
        public static final String COLUMN_DATE = "completedDate";
    }
}
