package com.example.quizapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

public class CustomAdapter extends ArrayAdapter<Score>{
    private Context sContext;
    private List<Score> scoreList = new ArrayList<>();

    public CustomAdapter(Context context,List<Score> list){
        super(context,0,list);
        sContext = context;
        scoreList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;

        if(listItem == null){
            listItem = LayoutInflater.from(sContext).inflate(R.layout.row_item,parent,false);
        }

        Score currentScore = scoreList.get(position);

        TextView scoreName = listItem.findViewById(R.id.rowItemName);
        scoreName.setText(currentScore.getPlayerName());

        TextView scorePoints = listItem.findViewById(R.id.rowItemScore);
        scorePoints.setText(String.valueOf(currentScore.getScore()));

        TextView scoreDate = listItem.findViewById(R.id.rowItemDate);
        scoreDate.setText(currentScore.getCompleted());

        return listItem;
        //return super.getView(position, convertView, parent);
    }
}
